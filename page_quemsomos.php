<main>
	<div class="content">
		
		
		<div class="PgQuemSomos">
			<h2>QUEM SOMOS</h2>
			
			<p>Fundado em 1982 pelo Dr. Walter Lopes Calvo, tornada empresa em 2011 com a entrada na sociedade pelo Dr. Douglas Sforsin Calvo, o escritório visa o encontro da realização dos negócios pelos clientes, de forma legal e segura.
				
				Localizada estrategicamente próximo ao metro, e a  poucos metros do forum do Jabaquara, formada por profissionais especializados nas mais complexas e atuais disciplinas do Direito Brasileiro, com especialização em Administração (MBA - FGV) para ter – mais do que o foco no cliente, mas o foco do cliente - , preparados através de continuada e permanente atualização para atender às necessidades dos clientes com presteza, precisão técnica e disponibilidade, a Calvo & Calvo Sociedade de Advogados oferece atendimento personalizado e de alta qualidade, na chama “Advocacia Artesanal” com vistas a prestar serviços legais diferenciados.
				
			Além das áreas tradicionais do Direito Trabalhista , Civil, Societário e Contratual, contandocom ampla estrutura para defender os interesses de seus clientes também nas áreas de Direito Bancário, Direito eletronico (B2B e B2C), Contratos Internacionais, e tantos outros de igual maneira sofisticados.</p>
			
			Com estratégia, concilia a advocacia contenciosa e preventiva, objetivando minimizar o impacto financeiro e burocrático de infindáveis demandas judiciais, viabilizando negócios de maneira segura e legal.
		</div>
	</div> 
</main>
<div class="clear"></div>