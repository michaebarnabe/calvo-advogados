
<header>
	<section class="logo">
		<img src="img/logo.jpg" alt="" class="logo" />
	</section>	
	<section class="banner">
		<nav>
			<ul>
				<li class="sembarra"><a href="?p=home">Inicial</a></li>
				<li><a href="?p=quemsomos">Quem Somos</a></li>
				<li><a href="?p=escritorio">Escritório</a></li>
				<li><a href="?p=atuacao">Áreas de Atuação</a></li>
				<li><a href="?p=blog">Blog</a></li>
				<li><a href="?p=contato">Contato</a></li>				
				
				
			</ul>
			<a href="#" class="menu-mobile">Menu</a>
		</nav>
		
		<article class="articleNoticias">
			<div class="noticias">
				<h1>Veja o que foi destaque no mês de novembro no TRT da 2ª Região</h1>
				<p>No mês de novembro/2016, vários eventos tiveram destaque no Tribunal Regional do Trabalho da 2ª Região. Segue abaixo um pequeno resumo do que foi realizado.<br>A Semana Nacional da Conciliação foi realizada entre 21 e 25 de novembro. No TRT-2, aconteceram cerca de 7 mil audiências, e mais de 2.200 resultaram em acordo. Os valores envolvidos ultrapassam R$ 50 milhões.</p>
				<a href="#" class="LinkNoticias">Leia +</a>
			</div>
		</article>
		<article class="articleNoticias">	
			<div class="noticias">
				<h1>Presidente do TRT-2 expõe preocupação com aprovação da PEC 55</h1>
				<p>Em 2016, a Justiça do Trabalho sofreu corte orçamentário superior a qualquer outro ramo do Poder Judiciário, chegando a 30% para despesas de custeio e 90% para investimentos. A medida só não inviabilizou o funcionamento dos tribunais trabalhistas por força de uma medida provisória, que concedeu a utilização de valores referentes a depósitos recursais como crédito extraordinário aos tribunais trabalhistas.</p>
				<a href="#" class="LinkNoticias">Leia +</a>
			</div>
		</article>	
		<article class="articleNoticias">
			<div class="noticias">
				<h1>Câmara aprova lei que cerceia atividade judicial</h1>
				<p>A Câmara dos Deputados aprovou nesta quarta-feira (30) o Projeto de Lei nº 4850/2016, que prevê casos de responsabilização de juízes e de membros do Ministério Público por crimes de abuso de autoridade. A proposta modifica lei anterior, passando a prever criminalização para diversas ações tomadas por magistrados.</p>
				<a href="#" class="LinkNoticias">Leia +</a>
			</div>			
		</article>
		<a href="#" class="chamada">Veja os artigos</a>
		<div class="clear"></div>
	</section>
</header>	