<main>
	<div class="content">
		<div class="Post">
			<h2 class="TituloPost">Veja o que foi destaque no mês de novembro no TRT da 2ª Região</h2>
			<p class="author"><strong>Por:</strong> Michael Pereira!!!!</p>
			<p class="DataPost">12/12/16 - <span class="cat">Categoria</span></p>
			<p class="TextPost">No mês de novembro/2016, vários eventos tiveram destaque no Tribunal Regional do Trabalho da 2ª Região. Segue abaixo um pequeno resumo do que foi realizado.
			A Semana Nacional da Conciliação foi realizada entre 21 e 25 de novembro. No TRT-2, aconteceram cerca de 7 mil audiências, e mais de 2.200 resultaram em acordo. Os valores envolvidos ultrapassam R$ 50 milhões.</p>
		</div>
		
		
		
		<div class="filtro-Post">
			<ul>
				<li>Últimas Publicações</li>
				<li><a href="#">Titulo 01</a></li>
				<li><a href="#">Titulo 02</a></li>			
			</ul>
			
			<ul>
				<li>Categorias</li>
				<li><a href="#">Titulo 01</a></li>
				<li><a href="#">Titulo 02</a></li>			
			</ul>
		</div>
		
		
		<div class="comaprtilhar">
			<ul>
				<li class="tituloCompartilha">COMPARTILHAR:</li>
				<li><a href=""><img src="img/icone/facebook.png" /></a></li>
				<li><a href=""><img src="img/icone/twitter.png" /></a></li>
				<li><a href=""><img src="img/icone/google.png" /></a></li>
				<li><a href=""><img src="img/icone/envelope.png" /></a></li>	
			</ul>
		</div>
		
	</div>
</main>
<div class="clear"></div>