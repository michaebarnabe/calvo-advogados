<main>
	<div class="content">
		<h2><center>ÁREAS DE ATUAÇÃO</center></h2>
		<hr class="linha">
		<div class="textAtuacao">
			<div class="open_div">
				<p> <span>+</span> Trabalhista</p>
			</div>
			<div class="hidden_div">
				<p>Consultoria em direito individual e coletivo do trabalho, abrangendo defesas em reclamações trabalhistas em todas as instâncias.</br> 
				Assessoria em direito sindical, negociação de convenção coletiva do trabalho de categorias profissionais, relações entre sindicato patronal e de empregados, análise do passivo trabalhista.</br> 
			Orientação preventiva e defesa em reclamações decorrentes de assédios moral e sexual.</p>
		</div>		
	</div>
	
	<div class="textAtuacao">
		<div class="open_div">
			<p><span>+</span> Trabalhista - Administrativa</p>
		</div>
		<div class="hidden_div">
			<p>Assessoria na condução de Procedimentos Investigatórios do Ministério Público do Trabalho, e procedimentos do Ministerio do Trabalho e Emprego (Autos de infração, ações anulatórias, Delegacia Regional do Trabalho). 
				Análise e elaboração de contratos administrativos e definição de estratégias de conduta de empresas perante a Administração Pública Direta e Indireta.
			Assessoria a empresas tocante aos aspectos contratuais, regulatórios e fiscais, .</p>
		</div>		
	</div>
	
	<div class="textAtuacao">
		<div class="open_div">
			<p><span>+</span>Cível</p>
		</div>
		<div class="hidden_div">
			<p>Defesa dos interesses de nossos clientes nas matérias relacionadas no Código Civil, inclusive, nos Títulos relacionados à Empresa e ao Empresário.
			Soluções em consultas e pareceres para conflitos em contratos, responsabilidade civil, recuperação de crédito, análise de riscos empresariais, resolução alternativa de conflitos, arbitragem e mediação.</p>
		</div>		
	</div>

	<div class="textAtuacao">
		<div class="open_div">
			<p><span>+</span> Relações de consumo</p>
		</div>
		<div class="hidden_div">
			<p>Consultoria nas relações de consumo tratadas pelo Código de Defesa do Consumidor, patrocinando a defesa dos interesses dos nossos clientes perante Procon’s, Juizados Especiais e Varas de Defesa do Consumidor.
				Elaboração de procedimentos de conduta, com treinamento de equipes, a fim de evitar demandas consumeristas. 
			Administração do passivo gerado por processos administrativos e judiciais envolvendo matérias do Direito do Consumidor.</p>
		</div>		
	</div>
	
	<div class="textAtuacao">
		<div class="open_div">
			<p><span>+</span> Contratual</p>
		</div>
		<div class="hidden_div">
			<p>Assessoria jurídica para o exame, revisão e negociação de contratos nacionais e internacionais em geral, tais como compra e venda, locação de bens móveis e imóveis, prestação de serviços, empreitada, arrendamento mercantil, fornecimento de bens e serviços, franquia, dentre outros</p>
		</div>		
	</div>
	
	<div class="textAtuacao">
		<div class="open_div">
			<p><span>+</span> Imobiliária</p>
		</div>
		<div class="hidden_div">
			<p>Consultoria na realização de negócios imobiliários, constituição de hipotecas, incorporações imobiliárias, implantação e regularização de condomínios residenciais, industriais, comerciais e hospitalares, loteamentos residenciais e mistos.</p>
		</div>		
	</div>
	
	<div class="textAtuacao">
		<div class="open_div">
			<p><span>+</span> Informática e internet</p>
		</div>
		<div class="hidden_div">
			<p>Resolução de conflitos envolvendo direito autoral de softwares, e-commerce/e-business, contratos firmados através da internet e suas consequências legais, virtual company, prestando assessoria jurídica a nossos clientes que têm suas atividades realizadas por intermédio da Internet.</p>
		</div>		
	</div>
	
	<div class="textAtuacao">
		<div class="open_div">
			<p><span>+</span> Penal empresarial</p>
		</div>
		<div class="hidden_div">
			<p>Defesa do empresário em inquéritos policiais e ações penais para apuração de crimes (Sonegação Fiscal, crimes contra o Sistema Financeiro, Evasão de Divisas, Relações de Consumo, Responsabilidade por Infidelidade Depositária relacionada a Tributos,  Contra a Honra).
			Adoção de providências legais quando da constatação de crime contra o empresário ou a empresa.</p>
		</div>		
	</div>
	
	<div class="textAtuacao">
		<div class="open_div">
			<p><span>+</span> Societária</p>
		</div>
		<div class="hidden_div">
			<p>Aconselhamento legal na área de negócios empresariais em geral.
				
				Assessoria para constituição e manutenção de sociedades, incluindo fusões, incorporações, cisões, liquidações, desinvestimentos, reorganizações societárias, aquisição de empresas, compra e venda de participações societárias, ativos, estabelecimentos comerciais e outros negócios empresariais. 
			Assessoria nas atividades de rotina das sociedades, como constituição e alteração de Contratos Sociais, governança corporativa, participação em assembleias e reuniões de conselhos de administração e fiscal de sociedades.</p>
		</div>		
	</div>
	
	<div class="textAtuacao">
		<div class="open_div">
			<p><span>+</span> Terceiro setor</p>
		</div>
		<div class="hidden_div">
			<p>Assessoria para desenvolvimento de projetos de investimento social, individualmente ou em parceira com instituições públicas ou privadas.
			Consultoria a empresas que tenham interesse em beneficiar-se do tratamento fiscal concedido a patrocinadores, doadores e investidores de eventos artísticos, culturais e esportivos.</p>
		</div>		
	</div>
	
	
	
	
	
	
	
	
</div>
</main>
<div class="clear"></div>	