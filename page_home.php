
<main>
	
	<div class="content">
		<div class="box-1-2">
			<div class="imagem">	
				<img src="img/img_9747.jpg" alt="Foto - Walter Lopes Calvo - Sócio"  />
			</div>
			<div class="texto">	
				<p><span class="nome">Douglas Sforsin Calvo - Sócio</span></br>
				(Comunica-se em Português, Inglês e Espanhol).</br></br>
				Advogado, Consultor de Relações Trabalhistas e Sindicais , bacharel em Direito pela FMU em 2002, MBA em Gestão Empresarial pela FGV e Pós-Graduação em Direito Processual Civil PUC.SP. Pós Graduado em Direito pela Faculdade de Direito Damásio Evangelista de Jesus.
				Diretor da Comissão de Direito do Trabalho, da OABSP Subseção Jabaquara.
				Liderança, habilidades de negociação e relacionamento e orientados para negócios.
				Relações de trabalho e sindicais. Antes foi Advogado Sênior, responsável por apoiar Recursos Humanos em procedimentos e orientações legais.
				Foi gestor de processos cíveis e trabalhistas de multinacional de telecomunicações (líder nacional), alimentos(líder mundial) e provedor de internet, segurança bancária e de construção civil e Outsourcing.
			Gestão de provisões e contingências, de acordo com as regras da CVM, atuando em conjunto com a contabilidade / auditoria (Legal Compliance, SOX).</p>
		</div>
	</div>

	
	<div class="box-2-2">
		<div class="imagem-dois">	
			<img src="img/img_9752.jpg" alt="Foto - Walter Lopes Calvo - Sócio"  />
		</div>
		<div class="texto">	
			<p><span class="nome">Walter Lopes Calvo - Sócio</span></br>
			(Comunica-se em Português e Espanhol).</br> </br>
			Advogado, Professor de Direito e Processo do Trabalho, formado em 1982 pela Faculdade de Direito Braz Cubas, Pós Graudado em Direito e Processo do Trabalho pela Universidade São Judas, Pós Graduado em Direito Empresarial pela UniFMU. 
			Acumulando mais de 30 anos na advocacia, defendendo interesses desde Multinacionais à pessoas físicas, nas áreas Trabalhista, Civel e Direito de Família. 
		Experiência, perspicácia e pronfundo conhecimento nas diversas ramificações do Direito, construíram sua imagem e confiabilidade de seus clientes.</p>
	</div>
</div>
</div>	  
</main>
<div class="clear"></div>	

