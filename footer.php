<section class="newslatter">	
	<form>
		<p>Receba nosso Newslatters</p>
		<input type="email" name="email"  maxlength="40" placeholder="Digite seu E-mail" class="formNews" />
		<input id="bnt" type="submit" value="Assine Aqui" class="btn"/>
	</form>
</section>
<footer>
	<div class="areafooter">
		<div class="menuFooter">
			<p class="TituloFooter">Menu</p>
			<ul>		
				<li><a href="?p=home" ><span class="seta">&#8227;</span>inicio</a></li>
				<li><a href="?p=quemsomos" ><span class="seta">&#8227;</span>Quem Somos</a></li>
				<li><a href="?p=escritorio" ><span class="seta">&#8227;</span>Escritório</a></li>
				<li><a href="?p=atuacao" ><span class="seta">&#8227;</span>Áreas de Atuação</a></li>
				<li><a href="?p=contato" ><span class="seta">&#8227;</span>Contato</a></li>
			</ul>
		</div>
		
		
		<div class="FaleconoscoFooter">
			<p class="TituloFooter">Fale Conosco</p>
			<ul>		
				<li><a href="#" ><span class="seta">&#8227;</span>+55 (11) 5585-0508</a></li>
				<li><a href="#" ><span class="seta">&#8227;</span>+55 (11) 5584-8928</a></li>
				<li><a href="#" ><span class="seta">&#8227;</span>+55 (11) 5581-6867</a></li>
				<li><a href="#" class="email" ><span class="seta">&#8227;</span>E-mail: contato@calvoadvogados.com.br</a></li>
			</ul>
		</div>
		
		<div class="FooterRedes">
			<p class="TituloFooter">Redes Sociais</p>
			<ul>		
				<li><a href="#" ><span class="seta">&#8227;</span>Facebook</a></li>
				<li><a href="#" ><span class="seta">&#8227;</span>Twitter</a></li>
				<li><a href="#" ><span class="seta">&#8227;</span>Linkedin</a></li>
				<li><a href="?p=blog" ><span class="seta">&#8227;</span>Blog</a></li>
			</ul>
		</div>
		<div class="EnderecoFooter">
			<p class="TituloFooter">Escritório 01</p>
			<p class="endereçoFooter">Endereço: R. Correia de Lemos,420 Bairro: Chárara Inglesa São Paulo - SP</br>Cep: 044140-000</p>
			
		</div>
		<div class="EnderecoFooter">
			<p class="TituloFooter">Escritório 02</p>
			<p class="endereçoFooter">Endereço: R. Correia de Lemos,420 Bairro: Chárara Inglesa São Paulo - SP</br>Cep: 044140-000</p>
			<span class="restrito"><a href="">Area Restrita<img src="img/cadeado.png" alt=""class="seguranca"/></a></span>
		</div>
		<div class="clear"></div>		
	</div>
	<div class="direitos">
		<address>copyright 2016 calvo advogados</address>
	</div>
</footer>	
