<!DOCTYPE html>
<html lang="pt-br">
<head>
	<title>Calvo e Calvo Advogados</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="css/css.css">
	<link rel="stylesheet" type="text/css" href="css/mobile.css">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
	<script type="text/javascript" src="js/js.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
	<link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
	
</head>
<body>

	<?php
	require_once('funcoes.php');
	require_once('header.php');
	carrega_pagina();
	require_once('footer.php');
	?>

</body>
</html>