<main>
	<div class="content">
		<h2><center>NOSSO ESCRITÓRIO</center></h2>
		<hr class="linha">
		<div class="galery">
			<ul>
				<li><a href="#img6"><img src="galeria/06.jpg" class="min"/></a></li>			
				<li><a href="#img2"><img src="galeria/02.jpg" class="min"/></a></li>
				<li><a href="#img4"><img src="galeria/04.jpg" class="min"/></a></li>
				<li><a href="#img5"><img src="galeria/05.jpg" class="min"/></a></li>
				<li><a href="#img3"><img src="galeria/03.jpg" class="min"/></a></li>
				<li><a href="#img1"><img src="galeria/01.jpg" class="min"/></a></li>
				
				
				
			</ul>
			<div class="lbox" id="img1">
				<div class="box_img">
					<a href="" class="btnf" id="prev">&#171;</a>
					<a href="" class="btnf" id="close">X</a>
					<img src="galeria/01.jpg" alt="" />
					<a href="" class="btnf" id="next">&#187;</a>
				</div>
			</div>
			<div class="lbox" id="img2">
				<div class="box_img">
					<a href="" class="btnf" id="prev">&#171;</a>
					<a href="" class="btnf" id="close">X</a>
					<img src="galeria/02.jpg" alt="" />
					<a href="" class="btnf" id="next">&#187;</a>
				</div>
			</div>
			<div class="lbox" id="img3">
				<div class="box_img">
					<a href="" class="btnf" id="prev">&#171;</a>
					<a href="" class="btnf" id="close">X</a>
					<img src="galeria/03.jpg" alt="" />
					<a href="" class="btnf" id="next">&#187;</a>
				</div>
			</div>
			<div class="lbox" id="img4">
				<div class="box_img">
					<a href="" class="btnf" id="prev">&#171;</a>
					<a href="" class="btnf" id="close">X</a>
					<img src="galeria/04.jpg" alt="" />
					<a href="" class="btnf" id="next">&#187;</a>
				</div>
			</div>
			<div class="lbox" id="img5">
				<div class="box_img">
					<a href="" class="btnf" id="prev">&#171;</a>
					<a href="" class="btnf" id="close">X</a>
					<img src="galeria/05.jpg" alt="" />
					<a href="" class="btnf" id="next">&#187;</a>
				</div>
			</div>
			<div class="lbox" id="img6">
				<div class="box_img">
					<a href="" class="btnf" id="prev">&#171;</a>
					<a href="" class="btnf" id="close">X</a>
					<img src="galeria/06.jpg" alt="" />
					<a href="" class="btnf" id="next">&#187;</a>
				</div>
			</div>
		</div>
	</div> 
</main>